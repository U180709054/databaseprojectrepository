# Show the employees whose salary is greater than 75000
select first_name, last_name
from person
where job_information_id in(
	select job_id
    from job_information
    where salary > 75000);

# Show the employees whose environment satisfaction is 4 and performance rating is 4

select first_name, last_name
from person
where job_information_id in(
	select job_id
    from job_information
    where environment_satisfaction = 4 and performance_rating = 4);
    
# Show the employees whose is year of birth greater than 1970 and weight in kgs  greater than 65.

select first_name, last_name
from person
where year_of_birth > 1970 and weight_in_kgs > 65;

# Show the male employees whose name prefix is prof and marital status is divorced or single.

select first_name, last_name
from person
where name_prefix = "Prof." and (marital_status = "Divorced" or marital_status = "Single") and gender = 'M';

# Show all the departments average salaries.

select department, avg(salary)
from job_information
group by department;