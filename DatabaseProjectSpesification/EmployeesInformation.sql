-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION';

-- -----------------------------------------------------
-- Schema company
-- -----------------------------------------------------
DROP SCHEMA IF EXISTS `company` ;

-- -----------------------------------------------------
-- Schema company
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `company` DEFAULT CHARACTER SET utf8 ;
USE `company` ;

-- -----------------------------------------------------
-- Table `communication_information`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `communication_information` ;

CREATE TABLE IF NOT EXISTS `communication_information` (
  `communication_id` INT NOT NULL AUTO_INCREMENT,
  `e-mail` VARCHAR(100) NOT NULL,
  `phone_no` VARCHAR(15) NOT NULL,
  `username` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`communication_id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `adress_information`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `adress_information` ;

CREATE TABLE IF NOT EXISTS `adress_information` (
  `address_id` INT NOT NULL AUTO_INCREMENT,
  `place_name` VARCHAR(45) NOT NULL,
  `country` VARCHAR(45) NOT NULL,
  `city` VARCHAR(2) NOT NULL,
  `zip` INT NOT NULL,
  `region` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`address_id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `job_information`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `job_information` ;

CREATE TABLE IF NOT EXISTS `job_information` (
  `job_id` INT NOT NULL,
  `department` VARCHAR(100) NOT NULL,
  `day_of_joining` INT NOT NULL,
  `month_of_joining` VARCHAR(45) NOT NULL,
  `year_of_joining` INT NOT NULL,
  `environment_satisfaction` INT NOT NULL,
  `salary` INT NOT NULL,
  `performance_rating` INT NOT NULL,
  `last_hike_percentages` VARCHAR(5) NOT NULL,
  PRIMARY KEY (`job_id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `person`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `person` ;

CREATE TABLE IF NOT EXISTS `person` (
  `person_id` INT NOT NULL,
  `communication_information_id` INT NOT NULL,
  `adress_information_id` INT NOT NULL,
  `job_information_id` INT NOT NULL,
  `name_prefix` VARCHAR(10) NOT NULL,
  `first_name` VARCHAR(45) NOT NULL,
  `last_name` VARCHAR(45) NOT NULL,
  `gender` CHAR NOT NULL,
  `father_name` VARCHAR(45) NOT NULL,
  `mother_name` VARCHAR(45) NOT NULL,
  `mother_maiden_name` VARCHAR(45) NOT NULL,
  `marital_status` VARCHAR(45) NOT NULL,
  `education_field` VARCHAR(100) NOT NULL,
  `day_of_birth` INT NOT NULL,
  `month_of_birth` VARCHAR(45) NOT NULL,
  `year_of_birth` INT NOT NULL,
  `weight_in_kgs` INT NOT NULL,
  `ssn` VARCHAR(45) NOT NULL,
  `password` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`person_id`, `communication_information_id`, `adress_information_id`, `job_information_id`),
  CONSTRAINT `fk_person_communication_information`
    FOREIGN KEY (`communication_information_id`)
    REFERENCES `communication_information` (`communication_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_person_adress_information1`
    FOREIGN KEY (`adress_information_id`)
    REFERENCES `adress_information` (`address_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_person_job_information1`
    FOREIGN KEY (`job_information_id`)
    REFERENCES `job_information` (`job_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;

-- -----------------------------------------------------
-- Data for table `communication_information`
-- -----------------------------------------------------
START TRANSACTION;
USE `company`;
INSERT INTO `communication_information` (`communication_id`, `e-mail`, `phone_no`, `username`) VALUES (1, 'angelique.goodwin@gmail.com', '212-884-7146', 'akgoodwin');

COMMIT;


-- -----------------------------------------------------
-- Data for table `adress_information`
-- -----------------------------------------------------
START TRANSACTION;
USE `company`;
INSERT INTO `adress_information` (`address_id`, `place_name`, `country`, `city`, `zip`, `region`) VALUES (1, 'Monroe', 'Rochester', 'NY', 14624, 'Northeast');

COMMIT;


-- -----------------------------------------------------
-- Data for table `job_information`
-- -----------------------------------------------------
START TRANSACTION;
USE `company`;
INSERT INTO `job_information` (`job_id`, `department`, `day_of_joining`, `month_of_joining`, `year_of_joining`, `environment_satisfaction`, `salary`, `performance_rating`, `last_hike_percentages`) VALUES (526540, 'Research Scientist', 24, 'March', 2001, 1, 193710, 1, '27%');

COMMIT;


-- -----------------------------------------------------
-- Data for table `person`
-- -----------------------------------------------------
START TRANSACTION;
USE `company`;
INSERT INTO `person` (`person_id`, `communication_information_id`, `adress_information_id`, `job_information_id`, `name_prefix`, `first_name`, `last_name`, `gender`, `father_name`, `mother_name`, `mother_maiden_name`, `marital_status`, `education_field`, `day_of_birth`, `month_of_birth`, `year_of_birth`, `weight_in_kgs`, `ssn`, `password`) VALUES (1, 1, 1, 526540, 'Ms.', 'Angelique', 'Goodwin', 'F', 'Rick Goodwin', 'Dorian Goodwin', 'Maxey', 'Married', 'Other', 15, 'May', 1964, 56, '471-57-0359', 'z{d>ez%{.@');

COMMIT;

